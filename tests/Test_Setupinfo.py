﻿'''
Created on 06.01.2018

@author: Christoph Jüngling
'''
import unittest
from setupinfo import SetupInfo, InnoSetupInfo, setup_factory


class TestSetupFactory(unittest.TestCase):

    def test_SetupFactory_NonExistent(self):
        with self.assertRaises(FileNotFoundError):
            _ = setup_factory('There ain\'t no such thing as a free software')

    def test_SetupFactory_IEData(self):
        # Use 'IEData' because this is supposed to be always there
        sinfo = setup_factory('IEData')
        self.assertEqual(type(sinfo).__name__, 'SetupInfo')

    def test_SetupFactory_Inno(self):
        # AccessMake is assumed to be installed. If not, this test will fail.
        sinfo = setup_factory('AccessMake_is1')
        self.assertEqual(type(sinfo).__name__, 'InnoSetupInfo')


class TestSetupInfo(unittest.TestCase):

    def test_SetupInfo_NonExistent(self):
        with self.assertRaises(FileNotFoundError):
            _ = SetupInfo('There ain\'t no such thing as a free lunch')

    def test_SetupInfo_IEData(self):
        # Use 'IEData' because this is supposed to be always there and empty
        sinfo = SetupInfo('IEData')
        self.assertEqual(sinfo.name, 'IEData', 'Name not matching')
        self.assertEqual(sinfo.registry_name, 'IEData',
                         'Registry name not matching')
        self.assertEqual(sinfo.setup, '', 'Setup defined, but it should not')
        self.assertEqual(sinfo.url, '', 'URL defined, but it should not')


class TestInnoSetupInfo(unittest.TestCase):

    def setUp(self):
        # TODO: Change to 'auto_is1' if the own installer is available
        self._sinfo = InnoSetupInfo('AccessMake_is1')

    def tearDown(self):
        del self._sinfo

    def test_SetupInfo_AccessMake_Name(self):
        self.assertEqual(self._sinfo.name, 'AccessMake')

    def test_SetupInfo_AccessMake_Setup(self):
        self.assertEqual(self._sinfo.setup, '/silent')

    def test_SetupInfo_AccessMake_Url(self):
        self.assertEqual(
            self._sinfo.url, 'https://gitlab.com/juengling/AccessMake/tags')

    def test_SetupInfo_AccessMake_RegName(self):
        self.assertEqual(self._sinfo.registry_name, 'AccessMake_is1')


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testSetupInfo']
    unittest.main()
