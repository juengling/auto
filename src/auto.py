from argparse import ArgumentParser
from configparser import ConfigParser
from datetime import datetime
from getpass import getuser
from os import getcwd, getlogin
from os.path import expandvars, dirname, join as pjoin
from socket import gethostname
import logging
import platform
import sys
import winreg

from setupinfo import setup_factory


__myname__ = 'auto'
__myshortname__ = 'auto'
__title__ = 'Automatic update tool'
__version__ = '0.2.2'


def main(argv):
    '''
    Automatic Update-Tool
    '''
    args = parse_command_line(argv)

    # Init logging
    if args.debug:
        logginglevel = logging.DEBUG
    else:
        logginglevel = logging.INFO

    logfilename = None
    if args.log:
        logfilename = getlogfilename()

    if args.logfile:
        logfilename = args.logfile

    if logfilename:
        print('Logging to ' + logfilename)
        logging.basicConfig(filename=logfilename,
                            format='%(asctime)s; %(levelname)s; %(name)s; %(message)s',
                            level=logginglevel)
    else:
        logging.basicConfig(level=logginglevel)

    logger = logging.getLogger('main')

    try:
        logger.debug('*' * 20 + 'Start' + '*' * 20)

        if args.scan:
            installed = scanner()
            create_config_file(installed, 'new_config_file.ini')
        else:
            with open(args.config, 'r') as configfile:
                config = configfile.read()

            programs = get_programs(config)
            uninstalled = check_installed(programs)
            logger.info('%d uninstalled programs', len(uninstalled))

        logger.debug('*' * 20 + 'Finished' + '*' * 20)

    except FileNotFoundError:
        logger.error('File %s not found!', args.config)
        return 2

    except Exception:
        logger.exception('Error')
        return 2


def parse_command_line(arguments):
    parser = ArgumentParser(prog=__title__ + ' v' +
                            __version__, description=main.__doc__)

    parser.add_argument('config', nargs='?', default=__myshortname__ + '.ini',
                        help='Configuration file for ' + __myshortname__ +
                        ' (default = ' + __myshortname__ + '.ini in the current folder).')

    parser.add_argument('--scan', action='store_true',
                        help='Scan current system and store data'
                        ' into new configuration file'
                        ' called "new_config_file.ini".')

#    parser.add_argument('-v', '--verbose', action='count', default=0,
#                        help='Print status information during work')

    parser.add_argument('--debug', action='store_true',
                        help='Print debug information during work')

    parser.add_argument('--log', action='store_true',
                        help='Activate log with default log file')

    parser.add_argument('--logfile',
                        help='Activate log with given name or path for the logfile')

    args = parser.parse_args(arguments)

    return args


def getlogfilename():
    '''
    Determine default log file location on several operating systems
    '''
    if platform.system() == 'Linux':
        return '/var/log/{0:s}.log'.format(__myname__)
    elif platform.system() == 'Windows':
        return r'C:\Users\{0:s}\AppData\Local\{1:s}.log'.format(getlogin(), __myname__)


def fullpath(filename):
    if dirname(filename) == '':
        # If no path exists, add current work directory
        return pjoin(getcwd(), filename)
    else:
        return filename


def get_programs(content):
    '''
    Read program settings from .ini file

    :param config: The content of the .ini file (not the filename!)
    '''

    logger = logging.getLogger('get_programs')
    logger.debug('Start reading config file ...')

    result = []
    config = ConfigParser()
    config.read_string(content)
    for program in config.sections():
        if program != 'AUTO-Options':
            p = {}
            p['name'] = program

            try:
                p['registry_name'] = config[program]['RegistryName']
            except KeyError:
                p['registry_name'] = program

            p['URL'] = config[program]['URL']
            p['Setup'] = config[program]['Setup']
            p['Settings'] = config[program]['Settings']
            result.append(p)

    logger.debug('Done, %d entries found.', len(result))
    return result


def get_installed_program_names():
    '''
    Get installed program names with a readable name (i.e. no {...})
    and return them as a list.

    :returns: list of installed programs with a readable name
    :rtype: list
    '''
    WINKEY = r'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall'

    logger = logging.getLogger('get_installed_program_names')
    logger.debug('Open Windows registry at HKLM\\%s', WINKEY)

    ignored_sw = read_ignored_software()

    result = []
    # In fact it scans
    # HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall
    # perhaps because this python is 32bit?

    installed = winreg.CreateKey(winreg.HKEY_LOCAL_MACHINE, WINKEY)

    i = 0
    while True:
        try:
            pgmname = winreg.EnumKey(installed, i)
            if not pgmname.startswith('{'):
                ignore = False
                for match in ignored_sw:
                    if pgmname.lower().startswith(match.lower()):
                        ignore = True
                        break
                if not ignore:
                    result.append(pgmname)
            i += 1
        except WindowsError:
            break

    msg = '{} program names found in registry'.format(len(result))
    logger.debug(msg)

    return result


def scanner():
    '''
    Scan registry, retrieve information on installed programs and return it

    :returns: list of dicts
    '''
    logger = logging.getLogger('scanner')
    logger.debug('Start scanner')

    result = []

    programs = get_installed_program_names()
    for program in programs:
        info = setup_factory(program)
        pgm = {'name': info.name,
               'registry_name': info.registry_name,
               'URL': info.url,
               'Setup': info.setup,
               'Settings': ''}
        result.append(pgm)

    logger.debug('Scan completed')
    print('Scan completed, {} program names found.'.format(len(result)))

    return result


def create_config_file(installed, filename):
    '''
    Create config file from list of installed programs.

    :param installed: List of installed programs
    :param filename: Destination file name
    '''

    logger = logging.getLogger('create_config_file')
    logger.debug('Write %d entries into %s ...', len(installed), filename)

    HEADER = '''# =======================
# AUTO configuration file
# =======================
#
# If "url" doesn't point to a downloadable file,
# download it manually and use "file=..." instead.

[AUTO-Options]
created={datetime}
user={username}
machine={hostname}

'''

    newfile = pjoin(getcwd(), filename)
    with open(newfile, 'w') as inifile:
        inifile.write(HEADER.format(
            datetime=datetime.today().strftime('%Y-%m-%dT%H:%M:%S'),
            username=getuser(),
            hostname=gethostname()))

        for program in installed:
            inifile.write('[{}]\n'.format(program['name']))
            inifile.write('RegistryName = {}\n'.format(
                program['registry_name']))
            inifile.write('url={}\n'.format(program['URL']))
            inifile.write('file=\n')
            inifile.write('setup={}\n'.format(program['Setup']))
            inifile.write('settings={}\n\n'.format(program['Settings']))

    logger.debug('Done.)')

    msg = 'Config file created: {}'.format(newfile)
    print(msg)


def check_installed(programs):
    '''
    Check if programs from list are installed

    :param programs(List of dict): Configured programs
    :returns: list of dicts with uninstalled programs
    '''

    logger = logging.getLogger('check_installed')

    result = []
    for program in programs:
        name = program['registry_name']
        logger.debug('Check installed %s ...', name)
        if is_installed(name):
            logger.debug('%s is installed', name)
        else:
            logger.debug('%s is not installed', name)
            logger.warning('%s is not installed', name)
            result.append(program)

    return result


def is_installed(program_name):
    '''
    Check if a program with the given name is installed.

    :param program_name: Name of the program
    '''

    try:
        _ = setup_factory(program_name)
        return True
    except FileNotFoundError:
        return False


def read_ignored_software():
    '''
    Read the list of software to be ignored from several potential positions.
    If nothing was found, return an empty list.

    :returns: List of software names to be ignored
    :rtype: list
    '''

    logger = logging.getLogger('read_ignored_software')
    logger.debug('Reading ignored software')

    paths = [getcwd(),
             pjoin(expandvars('%appdata%'), __myname__),
             pjoin(expandvars('%localappdata%'), __myname__),
             pjoin(expandvars('%programdata%'), __myname__)]
    result = []

    content = None
    for path in paths:
        filename = pjoin(path, 'ignore.ini')
        try:
            with open(filename, 'r') as file:
                content = file.read()
                logger.debug('Found in %s', path)
                break
        except FileNotFoundError:
            logger.debug('Not found in %s', path)

    if content:
        config = ConfigParser()
        config.read_string(content)
        for filename in config['Ignore']:
            result.append(filename)

    logger.debug('%d entries in ignore list: %s',
                 len(result), ', '.join(result))

    return result


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
