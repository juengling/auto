'''
Classes to handle setup information from registry
'''

from winreg import OpenKey, QueryValueEx, CreateKey, HKEY_LOCAL_MACHINE
import logging
import winreg


def setup_factory(registry_name):
    '''
    Factory to return the class best fitting for `registry_name`.

    :param str registry_name: Name of the program as used for registry key
    '''

    # First create the generic instance
    info = Generic(registry_name)

    # Now determine if any more specific class may do the job better
    known_software = {'HTML Help Workshop': HTML_Help_Workshop,
                      'KDiff3': KDiff3,
                      'MarkDownPad': MarkDownPad,
                      'Notepad++': NotepadPlusPlus,
                      'VeraCrypt': VeraCrypt}
    specific = None
    for software in known_software:
        if registry_name.lower().startswith(software.lower()):
            specific = known_software[software]

    if specific:
        return specific(registry_name)

    # If we could not determine any particular software,
    # we now try at least to identify the installer
    try:
        uninst = info.settings['UninstallString']
    except KeyError:
        uninst = None

    if registry_name.endswith('_is1'):
        return InnoSetupInstaller(registry_name)
    elif uninst and 'msiexec' in uninst:
        return MSIInstaller(registry_name)

    # No specific class and installer found, then return the Generic instance
    return info


##############################################################
# Generic installer
##############################################################


class Generic(object):
    '''
    Get default setup info from installed program.

    This class performs any default operation needed for most programs.
    Subclass it and override any method you need.

    :param str program_name: Name of the program
    '''

    WINKEY = r'SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall'

    def __init__(self, registry_name):
        self._logger = logging.getLogger(self.__class__.__name__)

        self._logger.debug('Retrieve settings')
        self.settings = self._get_settings(registry_name)

        self._logger.debug('Construct readable name')
        self.name = self._get_readable_name(registry_name)
        self.registry_name = registry_name

        self._logger.debug(
            'Construct registry key with the program\'s uninstall settings')
        base_reg_key = self._get_base_regkey()
        pgm_key = self._get_winkey(registry_name, base_reg_key)

        self._logger.debug('Read UpdateInfo')
        try:
            self.url = QueryValueEx(pgm_key, 'URLUpdateInfo')[0]
            self._logger.debug('URL = %s', self.url)
        except FileNotFoundError:
            self.url = ''
            self._logger.debug('No URL found.')

        self.setup = ''

    def _get_base_regkey(self):
        self._logger.debug('Open HKLM\\%s', self.WINKEY)
        installed = CreateKey(HKEY_LOCAL_MACHINE, self.WINKEY)
        return installed

    def _get_winkey(self, program_name, installed):
        self._logger.debug('Open key for %s', program_name)
        key = OpenKey(installed, program_name)
        return key

    def _get_readable_name(self, program_name):
        '''
        Default behaviour. Override if you need any specific thing.

        :param program_name: Name of the program
        '''

        try:
            if self.settings['DisplayName']:
                return self.settings['DisplayName']
            else:
                return program_name
        except KeyError:
            return program_name

    def _get_settings(self, basekey):
        '''
        Get and return the registry settings under the `HKLM/basekey`

        :param basekey: Basic registry key
        :returns: key-value pairs of the registry settings
        :rtype: dict
        '''

        result = {}
        i = 0
        # HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\AccessMake_is1

        basic = winreg.CreateKey(winreg.HKEY_LOCAL_MACHINE,
                                 self.WINKEY + '\\' + basekey)
        while True:
            try:
                key, value, typecode = winreg.EnumValue(basic, i)
                #self._logger.debug('%s: %s=%s (%d)', basekey, key, str(value), typecode)
                result[key] = value
                i += 1
            except WindowsError:
                break

        return result

    def _get_updateinfo(self, program_name):
        '''
        Default behaviour. Override if you need any specific thing.

        :param program_name: Name of the program
        '''
        return ''

    def add_setup_options(self, options):
        '''
        Default behaviour. Override if you need any specific thing.

        :param str options: Options to add for setup execution
        '''

        self._logger.debug('Add setup option(s)')
        self.setup += options


##############################################################
# Specific installers
##############################################################


class InnoSetupInstaller(Generic):
    '''
    Special class to retrieve information from a program installed by InnoSetup
    (http://innosetup.org).

    :param str program_name: Name of the program
    '''

    def __init__(self, program_name):
        super().__init__(program_name)

        self.add_setup_options('/silent')

    def _get_winkey(self, program_name, installed):
        try:
            key = OpenKey(installed, program_name)
        except FileNotFoundError:
            key = OpenKey(installed, program_name + '_is1')
        return key


class MSIInstaller(Generic):
    '''
    Class to handle setups installed by Microsoft Installer (MSI)
    '''

    def __init__(self, program_name):
        '''
        Constructor

        :param program_name: Name of the program
        '''
        super().__init__(program_name)

        self.add_setup_options('/Q')

    def _get_readable_name(self, program_name):
        '''
        Special behaviour: Strip '_is1' at the end, if present, to create a readable name.

        :param str program_name: Name of the program
        '''

        point = program_name.find('.')

        if point >= 0:
            return program_name[:point]
        else:
            return program_name


class NullsoftInstaller(Generic):
    '''
    Class to handle setups installed by Nullsoft (http://nsis.sourceforge.net)
    '''

    def __init__(self, program_name):
        '''
        Constructor

        :param program_name: Name of the program
        '''
        super().__init__(program_name)

        self.add_setup_options('/S')


##############################################################
# Specific Programs
##############################################################


class HTML_Help_Workshop(Generic):
    def __init__(self, program_name):
        super().__init__(program_name)

        self.url = 'https://download.microsoft.com/download/0/A/9/0A939EF6-E31C-430F-A3DF-DFAE7960D564/htmlhelp.exe'
        self.add_setup_options('/Q')


class KDiff3(NullsoftInstaller):
    def __init__(self, program_name):
        super().__init__(program_name)

        self.url = 'https://sourceforge.net/projects/kdiff3/files/kdiff3/'


class MarkDownPad(Generic):
    def __init__(self, program_name):
        super().__init__(program_name)

        self.url = 'http://markdownpad.com/download.html'


class NotepadPlusPlus(Generic):
    def __init__(self, program_name):
        super().__init__(program_name)

        self.url = 'https://notepad-plus-plus.org/download/'


class VeraCrypt(Generic):
    def __init__(self, program_name):
        super().__init__(program_name)

        self.url = 'https://www.idrix.fr/Root/content/category/7/32/46/'
